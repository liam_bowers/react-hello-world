import React from 'react';
import Hello from '$/components/Hello';

const App = () => (
  <div>
    <Hello name="foo" />
    <Hello name="bah" />
  </div>
);

export default App;
