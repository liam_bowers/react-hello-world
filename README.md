React Hello World

This is a very basic app designed to get you up and running with React. 

Note: This will need to be served from a webserver when not in dev mode.

To start, run:

npm install

npm run dev
- This will start a webserver running on http://localhost:8080 to allow you to see any changes.

npm run build
- This will create a production ready javascript file, ready to be served

npm run build-dev
- This will create a development javascript file, showing you any linting issues

npm run lint
- This will show you linting issues

npm run lint-fix
- This will try to fix linting issues where possible