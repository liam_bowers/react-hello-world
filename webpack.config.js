// GUI webpack.config.js
const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
  target: 'web',
  externals: [],
  mode: 'development',
  entry: {
    gui: `${path.resolve(__dirname, 'src')}/app.js`
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'eslint-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/react'],
            plugins: [
              '@babel/plugin-transform-runtime',
              '@babel/plugin-proposal-class-properties',
              [
                'babel-plugin-root-import',
                {
                  rootPathPrefix: '$',
                  rootPathSuffix: 'src'
                }
              ]
            ]
          }
        }
      },

      {
        test: /\.(scss|css)$/,
        resolve: { extensions: ['.scss', '.css'] },
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'resolve-url-loader?sourceMap',
          'sass-loader?sourceMap'
        ]
      },
      // "file" loader makes sure those assets get served by WebpackDevServer.
      // When you `import` an asset, you get its (virtual) filename.
      // In production, they would get copied to the `build` folder.
      // This loader doesn't use a "test" so it will catch all modules
      // that fall through the other loaders.
      {
        // Exclude `js` files to keep "css" loader working as it injects
        // its runtime that would otherwise processed through "file" loader.
        // Also exclude `html` and `json` extensions so they get processed
        // by webpacks internal loaders.
        test: /\.(png|jpg|gif|svg|woff|woff2|eot|ttf)$/,
        exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/],
        loader: 'file-loader',
        options: {}
      }
    ]
  },
  plugins: [
    new webpack.SourceMapDevToolPlugin({
      filename: '[name].js.map'
    }),

    // Clear out the public directory
    new CleanWebpackPlugin(['dist']),

    // Extract the CSS in to appropriate files
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: devMode ? 'styles/[name].css' : 'styles/[name].[hash].css',
      chunkFilename: devMode ? 'styles/[id].css' : 'styles/[id].[hash].css'
    }),

    // Build the html and inject the js and compiled css.
    new HtmlWebpackPlugin({
      template: 'src/template/index.html',
      filename: 'index.html'
    }),

    // Copy all of the other files needed for the front end to render correctly
    new CopyWebpackPlugin([{ from: path.resolve(__dirname, 'src/public') }], { debug: 'debug' })
  ]
};
